require('./models/db');
require('./auth/passportConfig');
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
const logger = require('morgan');
const passport = require('passport');
const app = express();
app.use(logger('dev'));

// API file for interacting with MongoDB
const userRouter = require('./routes/api/user');

// Parsers
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

// initialize passport
app.use(passport.initialize());

// Angular dist output folder
app.use(express.static(path.join(__dirname, 'dist', 'tacocat')));

// Enable CORS
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, NoAuth");
    next();
});

// API location
app.use('/api/users', userRouter);

// Send all other requests to the Angular app
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/tacocat/index.html'));
});

//Set Port
const port = process.env.PORT || '3000';
app.set('port', port);

const server = http.createServer(app);

server.listen(port, () => console.log(`Running on localhost:${port}`));