const mongoose = require('mongoose');
const config = require('../config');

mongoose.connect(config.url, (err) => {
    if (!err) { console.log('MongoDB connection succeeded.'); }
    else { console.log('Error in MongoDB connection : ' + JSON.stringify(err, undefined, 2)); }
});

require('./user');