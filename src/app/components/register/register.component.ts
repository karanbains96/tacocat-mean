import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../models/User';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user:User = {
    username: '',
    password: ''
  };
  fetching:boolean = false;

  constructor(public userService: UserService, public snackBar:MatSnackBar) { }

  ngOnInit() {
  }

  registerUser() {
    this.fetching = true;
    this.userService.registerUser(this.user).subscribe(
      (res) => {
        console.log(res);
        this.openSnackBar("User Registered!", "OK");
    }, (err) => {
        console.log(err);
        this.openSnackBar("Username already exists!", "OK");
    });
    this.fetching = false;
  }

  openSnackBar(msg, action) {
    this.snackBar.open(msg, action, {
      duration: 5000
    });
  }
}
