import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../models/User';
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user:User = {
    username: '',
    password: ''
  };
  fetching:boolean = false;
  error:boolean = false;
  serverErrorMessage:string;

  constructor(public userService:UserService, private router:Router) { }

  ngOnInit() {
  }

  login() {
    this.fetching = true;
    this.userService.login(this.user).subscribe(
      (res) => {
        console.log(res);
        console.log(res['token']);
        this.userService.setToken(res['token']);
        this.router.navigateByUrl('/dashboard');
      },
      (err) => {
        this.serverErrorMessage = err.error.message;
        this.error = true;
      }
    );
    this.fetching = false;
  }

}
